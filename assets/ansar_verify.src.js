/**
 * Created by kahos on 20/09/15.
 */
//toastr.success('Responsive Admin Theme', 'Welcome to INSPINIA');



var estados= "TODOS";
var searchButton ;
var textToSearch;
var listado;
var options;

function verificarCompleto(arreglo){

    var estado = true;

    for (i in arreglo){
        var texto = '<div id="idalertdng'+i+'" class="alert alert-danger alert-dismissable">'+
            '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
            'Este comportamiento no ha sido evaluado. ' +

            '</div>';

        console.log("Cpt:"+i+" est:"+arreglo[i]);
        if(arreglo[i]==0) {
            $( "#idtcp"+i ).append( texto );
            estado = false;
        }
    }
    return estado;
}


function seccionVisible(seccion, estado){
    var sector = "#seccion-"+seccion;
    if (estado == 1){
        $(sector).show("slow");
        console.log("mostrando");
    }else {
        $(sector).hide("fast");
        console.log("ocultando");
    }
}

var buscadorLista =function(options){

    searchButton = $('#'+options.id+' .cont-busqueda i.icono-buscar');
    textToSearch = $('#input'+options.id);
    listado = $('#list'+options.id);

    var errorSpan = $('#'+options.id+' span.error');
    console.log(options.lista);

    $("[name*=comport]").click(function(){

        var comport = $(this).attr('name').replace("comport-", "");
        var ident = $(this).val();
        if ($('input[name="comport-'+comport+'"]').not(':checked')) {
            console.log('Campo correcto');
            console.log('idt'+options.idt);
            options.lista[comport]=1;
        }
        else {
            console.log('Se debe seleccionar un idioma');
        }

        $.ajax({
            method: "POST",
            dataType: "json",
            url: options.url,
            data: { ct: comport, ids: ident , ide: options.ideval, idt: options.idt, dp: options.dp},
            success: function(result){
                console.log(result);
                if(result instanceof Array) {
                    if(result.length == 0) {
                        $("#idalertdng" + comport).remove();
                    }else {
                        console.log("malo");
                        toastr.error('Ha ocurrido un error!!', 'Por favor recargue la página');
                        options.lista[comport]=0;
                    }
                }else{
                    console.log("malo");
                    toastr.error('Ha ocurrido un error!!', 'Por favor recargue la página');
                    options.lista[comport]=0;
                }
            },
            error: function(result){
                console.log(result);
                console.log("malo");
                toastr.error('Ha ocurrido un error!!', 'Por favor recargue la página');
                options.lista[comport]=0;

            }
        });

    });

    searchButton.click(function(){

        errorSpan.html('');
        if(txt.length > 0)
        {
            console.log(options.url);
            cargarNodos(estados);

        }
    });

    $('#gwt-fmt').click(function(event){

        event.preventDefault();
        console.log("prevenido");
        errorSpan.html('');

        if(!verificarCompleto(options.lista))
        {
            toastr.error('Atención!!', 'Existen elementos sin evaluar');
            console.log('Atención!! Existen elementos sin evaluar');
           //cargarNodos(estados);

        }else{

            $.ajax({
                method: "POST",
                dataType: "json",
                url: options.close,
                data: { ide: options.ideval},
                success: function(result){
                    console.log(result);
                    window.location.href=options.urlsuccess;
                },
                error: function(result){
                    console.log(result);
                    toastr.error('Error!!', 'Ha ocurrido un error');
                }
            });

        }
    });

    $('button[name=estado]').click(function () {
        $('button[name=estado]').removeClass('active');
        $(this).addClass('active');
        estados=$(this).text();
        console.warn($(this).text());
        cargarNodos();
    });

    $('input[name*=radsecc]').click(function () {

        var seccion = $(this).attr('name').replace("radsecc-", "");
        var inputrad = "input[name='radsecc-"+seccion+"']";
        var estado = $(this).val();
        $(inputrad).prop("disabled", true);
        var sector = "#seccion-"+seccion;

        if (estado == 1){
            $(sector).show("slow");

        }else {
            var oculto = options.seccions[seccion];
            for (i in oculto) {
                options.lista[oculto[i]]=1;
                console.log("nodo "+oculto[i]);
            }
            $(sector).hide("slow");
        }
    });



};