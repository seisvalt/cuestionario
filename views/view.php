<?php
/**
 * Created by Jose Luis Vargas Solarte.
 * User: seisvalt
 * Date: 16/03/15
 * Time: 09:24 AM
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


$form = ActiveForm::begin();
foreach ($competencias as $comp) {
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h4><?=$comp->idCompetencia->nombre ?></h4>
                </div>
                <div class="ibox-content">
                    <p >
                        <?=$comp->idCompetencia->descripcion; ?>
                    </p>
                    <div class="body-content">
                        <div class="row">
                            <?php
                            $cont =1;
                            foreach ($comp->comportamientos as $cp) {

                                $nivel1 = $cp->nivel1;
                                $nivel2 = $cp->nivel2;
                                $nivel3 = $cp->nivel3;
                                $columnas ="col-lg-4";

                                if($comp->idCompetencia->nombre==''|| $comp->idCompetencia->nombre==null){
                                    $nivel1 = "Tiene el conocimiento básico exigidos por su cargo";
                                    $nivel2 = "Tiene dominio de los conocimientos exigidos y las implicaciones en su cargo.";
                                    $nivel3 = "Tiene dominio sobre los temas exigidos por su cargo y las implicaciones en el negocio. Su fortaleza la utiliza para ense?ar a otros.";
                                    $columnas = "col-lg-12";
                                }
                                ?>
                                <div class=<?= $columnas?>>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <?=$comp->idCompetencia->nombre ?> -  <?=$cp->nombre?>
                                        </div>
                                        <div class="panel-body" id="idtcp<?= $cp->id ?>">
                                            <?php
                                                $check[5]= isset($resultados[$cp->id])?:false;
                                                if($check[5])
                                                    $check[5] = ($resultados[$cp->id]==5)?true:false;
                                                $check[4]= isset($resultados[$cp->id])?:false;
                                                if($check[4])
                                                    $check[4] = ($resultados[$cp->id]==4)?true:false;
                                                $check[3]= isset($resultados[$cp->id])?:false;
                                                if($check[3])
                                                    $check[3] = ($resultados[$cp->id]==3)?true:false;
                                                $check[2]= isset($resultados[$cp->id])?:false;
                                                if($check[2])
                                                    $check[2] = ($resultados[$cp->id]==2)?true:false;
                                                $check[0]= isset($resultados[$cp->id])?:false;
                                                if($check[0])
                                                    $check[0] = ($resultados[$cp->id]==0)?true:false;

                                                $ramdon =[];
                                                $ramdon[]=Html::radio("comport-$cp->id",
                                                    $check[3],['label'=>$nivel1, 'value'=>3]);
                                                $ramdon[]=Html::radio("comport-$cp->id",
                                                    $check[4]?true:false,['label'=>$nivel2, 'value'=>4]);
                                                $ramdon[]=Html::radio("comport-$cp->id",
                                                    $check[5],['label'=>$nivel3, 'value'=>5]);
                                                shuffle($ramdon);
                                                echo "<p>".$ramdon[0]."</p>";
                                                echo "<p>".$ramdon[1]."</p>";
                                                echo "<p>".$ramdon[2]."</p>";
                                            ?>
                                            <p><?= Html::radio("comport-$cp->id",
                                                    $check[0],
                                                    ['label'=>"No tengo elementos para evaluar", 'value'=>0]);?>
                                            </p>
                                            <p><?= Html::radio("comport-$cp->id",
                                                    $check[2],
                                                    ['label'=>"No tiene desarrollada la competencia", 'value'=>2]);?>
                                            </p>
                                        </div>

                                    </div>

                                </div>
                                <?php
                                $cont++;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
echo Html::a('Finalizar y Enviar', ['fin'], ['class'=>'btn btn-primary', 
'id'=>'gwt-fmt']);
ActiveForm::end();
?>
