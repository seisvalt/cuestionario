

<?php
/**
 * Created by Jose Luis Vargas Solarte.
 * User: seisvalt
 * Date: 16/03/15
 * Time: 09:24 AM
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin();
$longitud = count($competencias);
$contador =0;
//Se ncrementan los valores en 1 para compatibilidad con ecom
$tipos = [
    "1"=>
        [
            1=>"Siempre",
            2=>"Casi Siempre",
            3=>"Algunas Veces",
            4=>"Casi Nunca",
            5=>"Nunca"
        ],
    "2"=>
        [
            5=>"Siempre",
            4=>"Casi Siempre",
            3=>"Algunas Veces",
            2=>"Casi Nunca",
            1=>"Nunca"
        ]
];
/*$respuestas = $tipos[$tipo];*/

while ($contador<$longitud-1){

    $comp = $competencias[$contador];
    $id_seccion = $comp->id_seccion;
    $display ="inline";

    if ($comp->idSeccion->optional == 1){
        $display ="none";
        echo "<p>".$comp->idSeccion->question."</p>";
        echo Html::radio("radsecc-$id_seccion",
            $check[$key],['label'=>"Si", 'value'=>"1",
               ]);
        echo Html::radio("radsecc-$id_seccion",
            $check[$key],['label'=>"No", 'value'=>"0",
                ]);
        echo "<br>";
    }
    ?>
    <div class="row" id="seccion-<?=$id_seccion;?>" style="display:<?=$display?>;">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h4><?=$comp->idSeccion->nombre ?></h4>
                </div>
                <div class="ibox-content">
                    <p></p>
                    <div class="body-content">
                        <div class="row">
                            <table id="" class="display compact" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>-</th>
                                    <th>Siempre</th>
                                    <th>Casi Siempre</th>
                                    <th>Algunas Veces</th>
                                    <th>Casi Nunca</th>
                                    <th>Nunca</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>No.</th>
                                    <th>-</th>
                                    <th>Siempre</th>
                                    <th>Casi Siempre</th>
                                    <th>Algunas Veces</th>
                                    <th>Casi Nunca</th>
                                    <th>Nunca</th>
                                </tr>
                                </tfoot>
                                <tbody>

                            <?php

                            $cont =1;

                            while ($id_seccion == $comp->id_seccion) {
                                $columnas ="col-lg-12";
                                $respuestas = $tipos[$comp->tipo];
                                $check[5]= isset
                                ($resultados[$comp->id_comportamiento])?:false;
                                if($check[5])
                                    $check[5] =
                                        ($resultados[$comp->id_comportamiento]==4)?true:false;
                                $check[4]= isset
                                ($resultados[$comp->id_comportamiento])?:false;
                                if($check[4])
                                    $check[4] =
                                        ($resultados[$comp->id_comportamiento]==3)?true:false;
                                $check[3]= isset
                                ($resultados[$comp->id_comportamiento])?:false;
                                if($check[3])
                                    $check[3] =
                                        ($resultados[$comp->id_comportamiento]==2)?true:false;
                                $check[2]= isset
                                ($resultados[$comp->id_comportamiento])?:false;
                                if($check[2])
                                    $check[2] =
                                        ($resultados[$comp->id_comportamiento]==1)?true:false;
                                $check[1]= isset
                                ($resultados[$comp->id_comportamiento])?:false;
                                if($check[1])
                                    $check[1]=($resultados[$comp->id_comportamiento]==0)?true:false;
                                echo "<tr>";
                                $cuenta = $contador+1;
                                echo "<td>$cuenta</td>";
                                $nombre= $comp->idComportamiento->nombre;
                                echo "<td>$nombre</td>";
                                    foreach($respuestas as $key => $value){
                                        echo "<td align='center'>";
                                        echo Html::radio("comport-$comp->id_comportamiento",
                                            $check[$key],['label'=>"", 'value'=>$key]);
                                        echo "</td>";
                                    }
                                echo "</tr>";
                                $cont++;
                                $contador++;
                                if($contador==$longitud)
                                    break;
                                $comp = $competencias[$contador];

                            }
                            ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
}
echo Html::a('Guardar y Enviar', ['fin'], ['class'=>'btn btn-primary', 'id'=>'gwt-fmt']);
ActiveForm::end();
?>
