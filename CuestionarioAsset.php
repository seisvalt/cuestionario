<?php
/**
 * Created by PhpStorm.
 * User: seisvalt
 * Date: 8/09/15
 * Time: 07:31 AM
 */

namespace seisvalt\lista_cuestionario;

use yii\web\AssetBundle;

class CuestionarioAsset extends AssetBundle
{

    public $sourcePath = '@vendor/seisvalt/yii2-cuestionario/assets';
    public $depends = ['yii\web\JqueryAsset'];

    /**
     * Registers additional JavaScript files required by the widget.
     *
     * @param array $scripts list of additional JavaScript files to register.
     * @return $this
     */
    public function withScripts($scripts = ['highcharts'])
    {
        // use unminified files when in debug mode
        $ext = YII_DEBUG ? 'src.js' : 'js';

        // add files
        foreach ($scripts as $script) {
            $this->js[] = "$script.$ext";
        }

        // make sure that either highcharts or highstock base file is included.
        array_unshift($this->js, "highcharts.$ext");
        $hasHighstock = in_array("highstock.$ext", $this->js);
        if ($hasHighstock) {
            array_unshift($this->js, "highstock.$ext");
            // remove highcharts if highstock is used on page
            $this->js = array_diff($this->js, ["highcharts.$ext"]);
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function init()
    {

        $ext = YII_DEBUG ? 'src.js' : 'js';

        $this->js[] = "ansar_verify.$ext";

                //
        //$this->js[] = "interaccion.$ext";
        //print_r($this->js);
        //}

        return $this;
    }


}