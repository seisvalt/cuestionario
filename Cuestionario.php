<?php
/**
 * Created by PhpStorm.
 * User: seisvalt
 * Date: 19/09/15
 * Time: 10:55 AM
 */
namespace seisvalt\lista_cuestionario;
use yii\base\Widget;
use Yii;
use yii\helpers\Json;
use yii\web\View;


class Cuestionario extends Widget{

	public $message;
	protected $constr = 'Chart';
	protected $baseScript = 'highcharts';
	private $id;
	public $options = [];
	public $data = [];
	public $model;
	public $htmlOptions = [];
	private $seccions = [];
	public $setupOptions = [];
	public $scripts = [];
	public $callback = false;
    private $title = "sensores";
	public $filter;
	public $view_render = "view";
	public $searchModel;
	public $attributes;
	public $ideval;
	public $idt;
	public $res;
	public $context;
	public $optionsJs;
    public $lista;
    public $url;
	public $dp;
	public $urlsuccess;
	public $urlclose;
	public $action=null;
	public $onSuccess;	// function(val,text){}
	public $onError;	// function(e){ alert(e.responseText); }
	public $onEmptyResult;	// function(txt){ }
	public $noResultsMessage='No results found.';


	public function init(){

		parent::init();
		if($this->message===null){
			$this->message= 'Se esperaba un dato';
		}
        if(is_array($this->options) && count($this->options) >= 1) {
            if (isset($this->options["title"]))
                $this->title = $this->options["title"];
        }
		$this->id = isset($htmlOption["id"])?$htmlOption["id"]:"list-".uniqid();
		if(is_array($this->data) && count($this->data) >= 1) {

			$modelAttributeId='';

			if ($this->view_render == "view") {
				foreach ($this->data as $comp) {
					foreach ($comp->comportamientos as $cp) {
						$this->lista[$cp->id]=(isset($this->res[$cp->id]))?1:0;
					}
				}
			}else{
				//Se establece a cero para compatibilidad con ecom
				$idseccion = -1;
				/*echo "<pre>";
				print_r($this->data);
				echo "</pre>";*/
				//echo sizeof($this->data);
				foreach ($this->data as $cp) {
					$this->lista[$cp->id_comportamiento] = 0;
					$this->lista[$cp->id_comportamiento]=(isset($this->res[$cp->id_comportamiento]))?1:0;
					$this->seccions[$cp->id_seccion][]=$cp->id_comportamiento;

				}
				//echo sizeof($this->lista);
			}

			$this->message= $this->render($this->view_render,
				[
					"competencias"=>$this->data,
					"resultados"=>$this->res,
					"tipo"=>$this->dp
				]);

			if(($this->model != null) && ($this->attribute != null))
				$modelAttributeId = get_class($this->model).'_'.$this->attribute;

			if($this->onSuccess == null)
				$this->onSuccess = 'function(val,text){ }';

			if($this->onError == null)
				$this->onError = 'function(e){ }';

			if($this->onEmptyResult == null)
				$this->onEmptyResult = 'function(txt){ }';

			
			$this->optionsJs = Json::encode(
				array(
					'id'=>$this->id,
                    'url'=>$this->url,
					'urlsuccess'=>$this->urlsuccess,
					'close'=>$this->urlclose,
					'onSuccess'=>$this->onSuccess,
					'onError'=>$this->onError,
					'onEmptyResult'=>$this->onEmptyResult,
					'modelAttributeId'=>$modelAttributeId,
					'noResultsMessage'=>$this->noResultsMessage,
                    'lista'=>$this->lista,
					'ideval'=>$this->ideval,
					'idt'=>$this->idt,
					'seccions'=>$this->seccions,
					'dp'=>$this->dp,
				)
			);
		}
		else{

			$this->message.= 'No se ha enviado un recurso de datos';


		}
	}

	/**
	 * Renders the widget.
	 */
	public function run(){
		$this->registerAssets();

		return $this->message;
	}


	/**
	 * Registers required assets and the executing code block with the view
	 */
	protected function registerAssets()
	{

		// register the necessary assets

		$js = "
		new buscadorLista({$this->optionsJs});";
		/*var nodo_select=0;
				$('a[id=$this->id]').click(
					function(){
						console.log('Llamando funcion centerLatLon con parametro '+$(this).attr('data-value'));
						centerLatLon($(this).attr('data-value'));
						return false;
						}
				);";*/
		$key = __CLASS__ . '#' . $this->id;
		$this->view->registerJs($js, View::POS_LOAD, $key);
		CuestionarioAsset::register($this->view);
	}
}
?>